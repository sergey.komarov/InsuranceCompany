﻿using System;
using If.InsuranceCompany.Core.ValueObjects;
using Xunit;

namespace If.InsuranceCompany.Tests
{
    public class DateRangeTests
    {
        [Fact]
        public void Includes_DateInRange_True()
        {
            // arrange
            var dateRange = new DateRange(DateTime.Now, 12);

            // act
            var result = dateRange.Includes(DateTime.Now.AddDays(1));

            // assert
            Assert.True(result);
        }

        [Fact]
        public void Includes_DateOutOfRange_False()
        {
            // arrange
            var dateRange = new DateRange(DateTime.Now, 12);

            // act
            var result1 = dateRange.Includes(DateTime.Now.AddDays(-1));
            var result2 = dateRange.Includes(DateTime.Now.AddYears(2));

            // assert
            Assert.False(result1);
            Assert.False(result2);
        }

        [Fact]
        public void Includes_RangeInRange_True()
        {
            // arrange
            var dateRange = new DateRange(DateTime.Now, 12);

            // act
            var childRange = new DateRange(DateTime.Now.AddDays(1), DateTime.Now.AddMonths(11));
            var result = dateRange.Includes(childRange);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void Includes_RangeOutOfRange_False()
        {
            // arrange
            var dateRange = new DateRange(DateTime.Now, 12);

            // act
            var rangeInPast = new DateRange(DateTime.Now.AddYears(-1), DateTime.Now.AddDays(-1));
            var rangeInFuture = new DateRange(DateTime.Now.AddYears(2), DateTime.Now.AddYears(3));
            var beginOverlappedRange = new DateRange(DateTime.Now.AddYears(-1), DateTime.Now.AddDays(1));
            var endOverlappedRange = new DateRange(DateTime.Now.AddMonths(11), DateTime.Now.AddYears(2));
            var fullOverlappedRange = new DateRange(DateTime.Now.AddDays(-1), DateTime.Now.AddYears(2));

            var rangeInPastResult = dateRange.Includes(rangeInPast);
            var rangeInFutureResult = dateRange.Includes(rangeInFuture);
            var beginOverlappedResult = dateRange.Includes(beginOverlappedRange);
            var endOverlappedResult = dateRange.Includes(endOverlappedRange);
            var fullOverlappedRangeResult = dateRange.Includes(fullOverlappedRange);

            // assert
            Assert.False(rangeInPastResult);
            Assert.False(rangeInFutureResult);
            Assert.False(beginOverlappedResult);
            Assert.False(endOverlappedResult);
            Assert.False(fullOverlappedRangeResult);
        }

        [Fact]
        public void Overlaps_RangeIntersectsRange_True()
        {
            // arrange
            var dateRange = new DateRange(DateTime.Now, 12);

            // act
            var includedRange = new DateRange(DateTime.Now.AddDays(1), DateTime.Now.AddMonths(11));
            var beginOverlappedRange = new DateRange(DateTime.Now.AddYears(-1), DateTime.Now.AddDays(1));
            var endOverlappedRange = new DateRange(DateTime.Now.AddMonths(11), DateTime.Now.AddYears(2));
            var fullOverlappedRange = new DateRange(DateTime.Now.AddDays(-1), DateTime.Now.AddYears(2));

            var includedRangeResult = dateRange.Overlaps(includedRange);
            var beginOverlappedResult = dateRange.Overlaps(beginOverlappedRange);
            var endOverlappedResult = dateRange.Overlaps(endOverlappedRange);
            var fullOverlappedRangeResult = dateRange.Overlaps(fullOverlappedRange);

            // assert
            Assert.True(includedRangeResult);
            Assert.True(beginOverlappedResult);
            Assert.True(endOverlappedResult);
            Assert.True(fullOverlappedRangeResult);
        }

        [Fact]
        public void Overlaps_NoIntersection_False()
        {
            // arrange
            var dateRange = new DateRange(DateTime.Now, 12);

            // act
            var rangeInPast = new DateRange(DateTime.Now.AddYears(-1), DateTime.Now.AddDays(-1));
            var rangeInFuture = new DateRange(DateTime.Now.AddYears(2), DateTime.Now.AddYears(3));

            var rangeInPastResult = dateRange.Includes(rangeInPast);
            var rangeInFutureResult = dateRange.Includes(rangeInFuture);

            // assert
            Assert.False(rangeInPastResult);
            Assert.False(rangeInFutureResult);
        }
    }
}
