﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using If.InsuranceCompany.Core.Exceptions;
using If.InsuranceCompany.Core.Managers;
using If.InsuranceCompany.Core.ValueObjects;
using If.InsuranceCompany.Core.ValueTypes;
using NSubstitute;
using Xunit;

namespace If.InsuranceCompany.Tests
{
    public class InsuranceCompanyTests
    {
        private readonly IFixture _fixture;

        public InsuranceCompanyTests()
        {
            _fixture = new Fixture();

            _fixture.Customize<InsuredRisk>(x => x
                .With(p => p.ValidFrom, DateTime.Now.AddDays(3))
                .With(p => p.ValidTill, DateTime.Now.AddDays(3).AddMonths(_fixture.Create<short>())));

            _fixture.Customize<Policy>(x => x
                .With(p => p.ValidFrom, DateTime.Now.AddDays(1))
                .With(p => p.ValidTill, DateTime.Now.AddDays(1).AddMonths(_fixture.Create<short>())));
        }

        #region [: SellPolicy :]

        [Fact]
        public void SellPolicy()
        {
            // arrange
            var policyManager = Substitute.For<IPolicyManager>();
            policyManager
                .PolicyExistsInGivenPeriod(Arg.Any<string>(), Arg.Any<DateTime>(), Arg.Any<short>())
                .Returns(false);
            policyManager
                .CreatePolicy(Arg.Any<string>(), Arg.Any<DateTime>(), Arg.Any<short>(), Arg.Any<IList<Risk>>())
                .Returns(_fixture.Create<Policy>());

            var sut = new Core.InsuranceCompany(policyManager);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = DateTime.Now.AddDays(1);
            var validMonths = _fixture.Create<short>();
            var risks = _fixture.CreateMany<Risk>().ToList();
            sut.AvailableRisks = risks;

            // act
            var ex = Record.Exception(() => sut.SellPolicy(nameOfInsuredObject, validFrom, validMonths, risks));

            // assert
            Assert.Null(ex);
        }

        [Fact]
        public void SellPolicy_NotAvailableRisk_ThrowException()
        {
            // arrange
            var policyManager = Substitute.For<IPolicyManager>();
            policyManager
                .PolicyExistsInGivenPeriod(Arg.Any<string>(), Arg.Any<DateTime>(), Arg.Any<short>())
                .Returns(false);
            policyManager
                .CreatePolicy(Arg.Any<string>(), Arg.Any<DateTime>(), Arg.Any<short>(), Arg.Any<IList<Risk>>())
                .Returns(_fixture.Create<Policy>());

            var sut = new Core.InsuranceCompany(policyManager);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = DateTime.Now.AddDays(1);
            var validMonths = _fixture.Create<short>();
            var risks = _fixture.CreateMany<Risk>().ToList();
            sut.AvailableRisks = risks;

            // act
            var ex = Record.Exception(() => sut.SellPolicy(nameOfInsuredObject, validFrom, validMonths, _fixture.CreateMany<Risk>().ToList()));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<NotAvailableRiskException>(ex);
        }

        #endregion

        #region [: AddRisk :]

        [Fact]
        public void AddRisk()
        {
            // arrange
            var policy = _fixture.Create<Policy>();
            var policyManager = Substitute.For<IPolicyManager>();
            policyManager
                .PolicyExistsOnEffectiveDate(Arg.Any<string>(), Arg.Any<DateTime>())
                .Returns(true);
            policyManager
                .GetPolicy(Arg.Any<string>(), Arg.Any<DateTime>())
                .Returns(policy);

            var sut = new Core.InsuranceCompany(policyManager);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = DateTime.Now.AddDays(1);
            var risks = _fixture.CreateMany<Risk>().ToList();
            var effectiveDate = DateTime.Now.AddDays(1);
            sut.AvailableRisks = risks;

            // act
            var ex = Record.Exception(() => sut.AddRisk(nameOfInsuredObject, risks[0], validFrom, effectiveDate));

            // assert
            Assert.Null(ex);
        }

        [Fact]
        public void AddRisk_RiskNotAvailable_ThrowException()
        {
            // arrange
            var policy = _fixture.Create<Policy>();
            var policyManager = Substitute.For<IPolicyManager>();
            policyManager
                .PolicyExistsOnEffectiveDate(Arg.Any<string>(), Arg.Any<DateTime>())
                .Returns(true);
            policyManager
                .GetPolicy(Arg.Any<string>(), Arg.Any<DateTime>())
                .Returns(policy);

            var sut = new Core.InsuranceCompany(policyManager);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = DateTime.Now.AddDays(1);
            var risks = _fixture.CreateMany<Risk>().ToList();
            var effectiveDate = DateTime.Now.AddDays(1);
            sut.AvailableRisks = risks;

            // act
            var ex = Record.Exception(() => sut.AddRisk(nameOfInsuredObject, _fixture.Create<Risk>(), validFrom, effectiveDate));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<NotAvailableRiskException>(ex);
        }

        #endregion

        [Fact]
        public void RemoveRisk()
        {
            // arrange
            var policy = _fixture.Create<Policy>();
            var policyManager = Substitute.For<IPolicyManager>();
            policyManager
                .RemoveRisk(Arg.Any<string>(), Arg.Any<Risk>(), Arg.Any<DateTime>(), Arg.Any<DateTime>());

            var sut = new Core.InsuranceCompany(policyManager);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = policy.ValidTill.AddDays(1);
            var risks = _fixture.CreateMany<Risk>().ToList();
            var effectiveDate = DateTime.Now.AddDays(1);
            sut.AvailableRisks = risks;

            // act
            var ex = Record.Exception(() => sut.RemoveRisk(nameOfInsuredObject, risks[0], validFrom, effectiveDate));

            // assert
            Assert.Null(ex);
        }

        [Fact]
        public void GetPolicy()
        {
            // arrange
            var policy = _fixture.Create<Policy>();
            var policyManager = Substitute.For<IPolicyManager>();
            policyManager
                .PolicyExistsOnEffectiveDate(Arg.Any<string>(), Arg.Any<DateTime>())
                .Returns(true);
            policyManager
                .GetPolicy(Arg.Any<string>(), Arg.Any<DateTime>())
                .Returns(policy);

            var sut = new Core.InsuranceCompany(policyManager);

            var nameOfInsuredObject = policy.NameOfInsuredObject;
            var effectiveDate = policy.EffectivePeriod.Start;

            // act
            var result = sut.GetPolicy(nameOfInsuredObject, effectiveDate);

            // assert
            Assert.NotNull(result);
            Assert.Equal(result.NameOfInsuredObject, policy.NameOfInsuredObject);
            Assert.Equal(result.InsuredRisks, policy.InsuredRisks);
            Assert.Equal(result.ValidFrom, policy.ValidFrom);
            Assert.Equal(result.ValidTill, policy.ValidTill);
            Assert.Equal(result.Premium, policy.Premium);
        }

        #region [: AvailableRisks :]

        // non unique risk
        [Fact]
        public void AvailableRisks_NonUniqueRisks_ThrowException()
        {
            // arrange
            var policyManager = Substitute.For<IPolicyManager>();
            var sut = new Core.InsuranceCompany(policyManager);
            var risk = _fixture.Create<Risk>();
            sut.AvailableRisks.Add(risk);
            sut.AvailableRisks.Add(risk);

            // act
            var ex = Record.Exception(() => sut.AvailableRisks);

            // assert
            Assert.NotNull(ex);
            Assert.IsType<NonUniqueRiskInCollectionException>(ex);
        }

        // non unique risk
        [Fact]
        public void AvailableRisks_UniqueRisks_NoException()
        {
            // arrange
            var policyManager = Substitute.For<IPolicyManager>();
            var sut = new Core.InsuranceCompany(policyManager);
            var risk1 = _fixture.Create<Risk>();
            sut.AvailableRisks.Add(risk1);
            var risk2 = _fixture.Create<Risk>();
            sut.AvailableRisks.Add(risk2);

            // act
            var ex = Record.Exception(() => sut.AvailableRisks);

            // assert
            Assert.Null(ex);
        }

        #endregion

        [Fact]
        public void Name()
        {
            // arrange
            var policyManager = Substitute.For<IPolicyManager>();
            var sut = new Core.InsuranceCompany(policyManager);

            // act
            var name = sut.Name;

            // assert
            Assert.Equal("Acme Insurance Corporation", name);
        }

    }
}
