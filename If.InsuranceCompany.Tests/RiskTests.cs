﻿using AutoFixture;
using If.InsuranceCompany.Core.Exceptions;
using If.InsuranceCompany.Core.ValueTypes;
using Xunit;

namespace If.InsuranceCompany.Tests
{
    public class RiskTests
    {
        private readonly IFixture _fixture;

        public RiskTests()
        {
            _fixture = new Fixture();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Name_IsNullOrEmpty_ThrowException(string riskName)
        {
            // arrange
            var yearlyPrice = _fixture.Create<decimal>();

            // act
            var ex1 = Record.Exception(() => new Risk(riskName, yearlyPrice));
            var ex2 = Record.Exception(() => new Risk { Name = riskName, YearlyPrice = yearlyPrice });

            // assert
            Assert.NotNull(ex1);
            Assert.NotNull(ex2);
            Assert.IsType<RiskNameIsNullOrEmptyException>(ex1);
            Assert.IsType<RiskNameIsNullOrEmptyException>(ex2);
        }

        [Fact]
        public void Name_IsNotNullOrEmpty_NoException()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();

            // act
            var ex1 = Record.Exception(() => new Risk(riskName, yearlyPrice));
            var ex2 = Record.Exception(() => new Risk { Name = riskName, YearlyPrice = yearlyPrice });

            // assert
            Assert.Null(ex1);
            Assert.Null(ex2);
        }

        [Fact]
        public void YearlyPrice_Negative_ThrowException()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = -1;

            // act
            var ex1 = Record.Exception(() => new Risk(riskName, yearlyPrice));
            var ex2 = Record.Exception(() => new Risk { Name = riskName, YearlyPrice = yearlyPrice });

            // assert
            Assert.NotNull(ex1);
            Assert.NotNull(ex2);
            Assert.IsType<NegativeYearlyPriceException>(ex1);
            Assert.IsType<NegativeYearlyPriceException>(ex2);
        }

        [Fact]
        public void YearlyPrice_NonNegative_NoException()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();

            // act
            var ex1 = Record.Exception(() => new Risk(riskName, yearlyPrice));
            var ex2 = Record.Exception(() => new Risk { Name = riskName, YearlyPrice = yearlyPrice });

            // assert
            Assert.Null(ex1);
            Assert.Null(ex2);
        }

        [Fact]
        public void Equals_NameEqualPriceEqual_True()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();
            var risk1 = new Risk(riskName, yearlyPrice);
            var risk2 = new Risk(riskName, yearlyPrice);

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void Equals_NameEqualPriceNotEqual_False()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice1 = _fixture.Create<decimal>();
            var yearlyPrice2 = _fixture.Create<decimal>();
            var risk1 = new Risk(riskName, yearlyPrice1);
            var risk2 = new Risk(riskName, yearlyPrice2);

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void Equals_NameNotEqualPriceEqual_False()
        {
            // arrange
            var riskName1 = _fixture.Create<string>();
            var riskName2 = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();
            var risk1 = new Risk(riskName1, yearlyPrice);
            var risk2 = new Risk(riskName2, yearlyPrice);

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void Equals_NameNotEqualPriceNotEqual_False()
        {
            // arrange
            var riskName1 = _fixture.Create<string>();
            var riskName2 = _fixture.Create<string>();
            var yearlyPrice1 = _fixture.Create<decimal>();
            var yearlyPrice2 = _fixture.Create<decimal>();
            var risk1 = new Risk(riskName1, yearlyPrice1);
            var risk2 = new Risk(riskName2, yearlyPrice2);

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void Equals_NameEqualPriceEqualAsObject_True()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();
            var risk1 = (object)(new Risk(riskName, yearlyPrice));
            var risk2 = (object)(new Risk(riskName, yearlyPrice));

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void Equals_NullObject_False()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();
            var risk = (object)(new Risk(riskName, yearlyPrice));

            // act
            var result = risk.Equals(null);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void Equals_NameNotEqualPriceEqualAsObject_False()
        {
            // arrange
            var riskName1 = _fixture.Create<string>();
            var riskName2 = _fixture.Create<string>();
            var yearlyPrice1 = _fixture.Create<decimal>();
            var yearlyPrice2 = _fixture.Create<decimal>();
            var risk1 = (object)(new Risk(riskName1, yearlyPrice1));
            var risk2 = (object)(new Risk(riskName2, yearlyPrice2));

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void Equals_NotRiskObject_False()
        {
            // arrange
            var riskName1 = _fixture.Create<string>();
            var yearlyPrice1 = _fixture.Create<decimal>();
            var risk1 = (object)(new Risk(riskName1, yearlyPrice1));
            var risk2 = (object)(_fixture.Create<string>());

            // act
            var result = risk1.Equals(risk2);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void GetHashCode_NameNotNull()
        {
            // arrange
            var riskName = _fixture.Create<string>();
            var yearlyPrice = _fixture.Create<decimal>();
            var risk = new Risk(riskName, yearlyPrice);

            // act
            var result = risk.GetHashCode();

            // assert
            var nameHash = risk.Name.GetHashCode();
            var priceHash = risk.YearlyPrice.GetHashCode();
            Assert.Equal((nameHash * 397) ^ priceHash, result);
        }
    }
}
