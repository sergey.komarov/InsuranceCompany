﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using If.InsuranceCompany.Core.Exceptions;
using If.InsuranceCompany.Core.Managers;
using If.InsuranceCompany.Core.ValueObjects;
using If.InsuranceCompany.Core.ValueTypes;
using Xunit;

namespace If.InsuranceCompany.Tests
{
    public class PolicyManagerTests
    {
        private readonly IFixture _fixture;

        public PolicyManagerTests()
        {
            _fixture = new Fixture();

            _fixture.Customize<InsuredRisk>(x => x
                .With(p => p.ValidFrom, DateTime.Now.AddDays(3))
                .With(p => p.ValidTill, DateTime.Now.AddDays(3).AddMonths(_fixture.Create<short>())));

            _fixture.Customize<Policy>(x => x
                .With(p => p.ValidFrom, DateTime.Now.AddDays(1))
                .With(p => p.ValidTill, DateTime.Now.AddDays(1).AddMonths(_fixture.Create<short>())));
        }

        [Fact]
        public void PolicyExistsInGivenPeriod_DifferentInsuredObject_CrossedPeriods_False()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = policies[0].ValidFrom;

            // act
            var result = sut.PolicyExistsInGivenPeriod(nameOfInsuredObject, validFrom, 1);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void PolicyExistsInGivenPeriod_SameInsuredObject_NonCrossedPeriods_False()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validFrom = policies[0].ValidTill;

            // act
            var result = sut.PolicyExistsInGivenPeriod(nameOfInsuredObject, validFrom, 1);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void PolicyExistsInGivenPeriod_SameInsuredObject_CrossedPeriods_True()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validFrom = policies[0].ValidFrom;

            // act
            var result = sut.PolicyExistsInGivenPeriod(nameOfInsuredObject, validFrom, 1);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void PolicyExistsOnEffectiveDate_DifferentInsuredObject_DateInPeriod_False()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var effectiveDate = policies[0].ValidFrom;

            // act
            var result = sut.PolicyExistsOnEffectiveDate(nameOfInsuredObject, effectiveDate);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void PolicyExistsOnEffectiveDate_SameInsuredObject_DateInPast_False()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var effectiveDate = policies[0].ValidFrom.AddDays(-1);

            // act
            var result = sut.PolicyExistsOnEffectiveDate(nameOfInsuredObject, effectiveDate);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void PolicyExistsOnEffectiveDate_SameInsuredObject_DateInFuture_False()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var effectiveDate = policies[0].ValidTill.AddDays(1);

            // act
            var result = sut.PolicyExistsOnEffectiveDate(nameOfInsuredObject, effectiveDate);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void PolicyExistsOnEffectiveDate_SameInsuredObject_DateInPeriod_True()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var effectiveDate = policies[0].ValidTill;

            // act
            var result = sut.PolicyExistsOnEffectiveDate(nameOfInsuredObject, effectiveDate);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void CalculatePremium()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>()
            {
                new Risk(_fixture.Create<string>(), 100),
                new Risk(_fixture.Create<string>(), 200),
                new Risk(_fixture.Create<string>(), 300)
            };
            var policy = new Policy(nameOfInsuredObject, validFrom, validMonths, risks);

            // act
            var result = sut.CalculatePremium(policy);

            // assert
            Assert.Equal(255m, result);
        }

        [Fact]
        public void CreatePolicy()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>()
            {
                new Risk(_fixture.Create<string>(), 100),
                new Risk(_fixture.Create<string>(), 200),
                new Risk(_fixture.Create<string>(), 300)
            };

            // act
            var result = sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, risks);

            // assert
            Assert.Equal(nameOfInsuredObject, result.NameOfInsuredObject);
            Assert.Equal(validFrom, result.ValidFrom);
            Assert.Equal(validFrom.AddMonths(validMonths), result.ValidTill);
            Assert.Equal(risks, result.InsuredRisks);
            Assert.Equal(255m, result.Premium);

            var storedPolicy = sut.Policies.Single(x => x.NameOfInsuredObject.Equals(nameOfInsuredObject));
            Assert.Equal(nameOfInsuredObject, storedPolicy.NameOfInsuredObject);
            Assert.Equal(validFrom, storedPolicy.ValidFrom);
            Assert.Equal(validFrom.AddMonths(validMonths), storedPolicy.ValidTill);
            Assert.Equal(risks, storedPolicy.InsuredRisks);
            Assert.Equal(255m, storedPolicy.Premium);
        }

        [Fact]
        public void CreatePolicy_EmptyRiskList_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>();

            // act
            var ex = Record.Exception(() => sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, risks));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<EmptyRiskListException>(ex);
        }

        [Fact]
        public void CreatePolicy_NullRiskList_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;

            // act
            var ex = Record.Exception(() => sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, null));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<NullRiskListException>(ex);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void CreatePolicy_InsuredObjectNameIsNullOrEmpty_ThrowException(string nameOfInsuredObject)
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>();

            // act
            var ex = Record.Exception(() => sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, risks));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<InsuredObjectNameIsNullOrEmptyException>(ex);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void CreatePolicy_NonPositiveMonthAmount_ThrowException(short validMonths)
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            var risks = new List<Risk>();

            // act
            var ex = Record.Exception(() => sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, risks));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<NonPositiveMonthAmountException>(ex);
        }

        [Fact]
        public void CreatePolicy_NonUniqueObjectInGivenPeriod_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validFrom = policies[0].ValidFrom;
            short validMonths = 1;
            var risks = _fixture.Create<IList<Risk>>();

            // act
            var ex = Record.Exception(() => sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, risks));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<NonUniqueObjectInGivenPeriodException>(ex);
        }

        [Fact]
        public void CreatePolicy_StartPolicyDateInPast_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = DateTime.Now.AddDays(-1);
            short validMonths = 1;
            var risks = _fixture.Create<IList<Risk>>();

            // act
            var ex = Record.Exception(() => sut.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, risks));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<StartPolicyDateInPastException>(ex);
        }

        [Fact]
        public void GetPolicy()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            // act
            var result = sut.GetPolicy(policies[0].NameOfInsuredObject, policies[0].ValidFrom);

            // assert
            Assert.Equal(policies[0].NameOfInsuredObject, result.NameOfInsuredObject);
            Assert.Equal(policies[0].ValidFrom, result.ValidFrom);
            Assert.Equal(policies[0].ValidTill, result.ValidTill);
            Assert.Equal(policies[0].InsuredRisks, result.InsuredRisks);
            Assert.Equal(policies[0].Premium, result.Premium);
        }

        [Fact]
        public void GetPolicy_NotFount_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            // act
            var ex = Record.Exception(() => sut.GetPolicy(_fixture.Create<string>(), _fixture.Create<DateTime>()));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<PolicyNotFoundException>(ex);
        }

        [Fact]
        public void AddRisk()
        {
            // arrange
            string nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>()
            {
                new Risk(_fixture.Create<string>(), 100),
                new Risk(_fixture.Create<string>(), 200),
                new Risk(_fixture.Create<string>(), 300)
            };
            var policy = new Policy(nameOfInsuredObject, validFrom, validMonths, risks);
            var policies = new List<Policy>() { policy };
            var sut = new PolicyManager(policies);

            var risk = new Risk(_fixture.Create<string>(), 360);
            var effectiveDate = new DateTime(2024, 6, 3);

            // act
            sut.AddRisk(nameOfInsuredObject, risk, effectiveDate, effectiveDate);

            // assert
            var result = sut.Policies.Single();
            Assert.NotNull(result);
            Assert.Equal(result.NameOfInsuredObject, nameOfInsuredObject);
            Assert.Equal(result.InsuredRisks, risks);
            Assert.Equal(result.ValidFrom, validFrom);
            Assert.Equal(result.ValidTill, validFrom.AddMonths(validMonths));
            Assert.Equal(377m, result.Premium);
        }

        [Fact]
        public void AddRisk_StartRiskDateInPast_ThrowException()
        {
            // arrange
            string nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>()
            {
                new Risk(_fixture.Create<string>(), 100),
                new Risk(_fixture.Create<string>(), 200),
                new Risk(_fixture.Create<string>(), 300)
            };
            var policy = new Policy(nameOfInsuredObject, validFrom, validMonths, risks);
            var policies = new List<Policy>() { policy };
            var sut = new PolicyManager(policies);

            var risk = new Risk(_fixture.Create<string>(), 360);
            var effectiveDate = new DateTime(2024, 6, 3);

            // act
            var ex = Record.Exception(() => sut.AddRisk(nameOfInsuredObject, risk, DateTime.Now.AddDays(-1), effectiveDate));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<StartRiskDateInPastException>(ex);
        }

        [Fact]
        public void RemoveRisk()
        {
            // arrange
            string nameOfInsuredObject = _fixture.Create<string>();
            var validFrom = new DateTime(2024, 5, 3);
            short validMonths = 5;
            var risks = new List<Risk>()
            {
                new Risk(_fixture.Create<string>(), 360),
                new Risk(_fixture.Create<string>(), 200),
                new Risk(_fixture.Create<string>(), 300)
            };
            var policy = new Policy(nameOfInsuredObject, validFrom, validMonths, risks);
            var policies = new List<Policy>() { policy };
            var sut = new PolicyManager(policies);

            var effectiveDate = new DateTime(2024, 6, 3);

            // act
            sut.RemoveRisk(nameOfInsuredObject, risks[0], validFrom.AddMonths(2), effectiveDate);

            // assert
            var result = sut.Policies.Single();
            Assert.NotNull(result);
            Assert.Equal(result.NameOfInsuredObject, nameOfInsuredObject);
            Assert.Equal(result.InsuredRisks, risks);
            Assert.Equal(result.ValidFrom, validFrom);
            Assert.Equal(result.ValidTill, validFrom.AddMonths(validMonths));
            Assert.Equal(273.5m, result.Premium);
        }

        [Fact]
        public void RemoveRisk_RiskNotFound_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validTill = policies[0].ActualInsuredRisks[0].ValidTill.AddDays(-1);
            var effectiveDate = policies[0].ValidFrom;

            // act
            var ex = Record.Exception(() => sut.RemoveRisk(nameOfInsuredObject, _fixture.Create<Risk>(), validTill, effectiveDate));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<RiskNotFoundException>(ex);
        }

        [Fact]
        public void RemoveRisk_RiskDateOutOfEffectivePeriod_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validTill = policies[0].ActualInsuredRisks[0].ValidFrom.AddDays(-1);
            var effectiveDate = policies[0].ValidFrom;

            // act
            var ex = Record.Exception(() => sut.RemoveRisk(nameOfInsuredObject, policies[0].ActualInsuredRisks[0].Risk, validTill, effectiveDate));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<EndRiskDateOutOfEffectivePeriodException>(ex);
        }

        [Fact]
        public void RemoveRisk_RiskWithValidEffectivePeriodNotFound_ThrowException()
        {
            // arrange
            var policy = _fixture.Create<Policy>();
            var risk1 = new InsuredRisk(_fixture.Create<Risk>(), policy.ValidFrom.AddMonths(1), policy.ValidTill);
            var risk2 = new InsuredRisk(_fixture.Create<Risk>(), policy.ValidFrom, policy.ValidTill.AddMonths(-1));
            policy.ActualInsuredRisks = new List<InsuredRisk>() { risk1, risk2 };
            var policies = new List<Policy>() { policy };
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validTill = policies[0].ActualInsuredRisks[0].ValidTill.AddDays(-1);
            var effectiveDate = policies[0].ValidFrom;

            // act
            var ex = Record.Exception(() => sut.RemoveRisk(nameOfInsuredObject, policies[0].ActualInsuredRisks[1].Risk, validTill, effectiveDate));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<RiskWithValidEffectivePeriodNotFoundException>(ex);
        }

        [Fact]
        public void RemoveRisk_EndRiskDateInPast_ThrowException()
        {
            // arrange
            var policies = _fixture.Create<IList<Policy>>();
            var sut = new PolicyManager(policies);

            var nameOfInsuredObject = policies[0].NameOfInsuredObject;
            var validTill = DateTime.Now.AddDays(-1);
            var effectiveDate = policies[0].ValidFrom;

            // act
            var ex = Record.Exception(() => sut.RemoveRisk(nameOfInsuredObject, _fixture.Create<Risk>(), validTill, effectiveDate));

            // assert
            Assert.NotNull(ex);
            Assert.IsType<EndRiskDateInPastException>(ex);
        }

        [Fact]
        public void Constructor()
        {
            // act
            var sut = new PolicyManager();

            // assert
            Assert.NotNull(sut.Policies);
        }
    }
}
