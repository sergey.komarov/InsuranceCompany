﻿using System;
using AutoFixture;
using If.InsuranceCompany.Core.Exceptions;
using Xunit;

namespace If.InsuranceCompany.Tests
{
    public class ExceptionsTest
    {
        private readonly IFixture _fixture;

        public ExceptionsTest()
        {
            _fixture = new Fixture();
        }

        [Fact]
        public void EndRiskDateOutOfEffectivePeriodException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new EndRiskDateOutOfEffectivePeriodException();
            var messageResult = new EndRiskDateOutOfEffectivePeriodException(message);
            var messageWithInnerExceptionResult = new EndRiskDateOutOfEffectivePeriodException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void EmptyRiskListException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new EmptyRiskListException();
            var messageResult = new EmptyRiskListException(message);
            var messageWithInnerExceptionResult = new EmptyRiskListException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void EndRiskDateInPastException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new EndRiskDateInPastException();
            var messageResult = new EndRiskDateInPastException(message);
            var messageWithInnerExceptionResult = new EndRiskDateInPastException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void InsuredObjectNameIsNullOrEmptyException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new InsuredObjectNameIsNullOrEmptyException();
            var messageResult = new InsuredObjectNameIsNullOrEmptyException(message);
            var messageWithInnerExceptionResult = new InsuredObjectNameIsNullOrEmptyException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void NonPositiveMonthAmountException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new NonPositiveMonthAmountException();
            var messageResult = new NonPositiveMonthAmountException(message);
            var messageWithInnerExceptionResult = new NonPositiveMonthAmountException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void NonUniqueObjectInGivenPeriodException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new NonUniqueObjectInGivenPeriodException();
            var messageResult = new NonUniqueObjectInGivenPeriodException(message);
            var messageWithInnerExceptionResult = new NonUniqueObjectInGivenPeriodException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void NotAvailableRiskException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new NotAvailableRiskException();
            var messageResult = new NotAvailableRiskException(message);
            var messageWithInnerExceptionResult = new NotAvailableRiskException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void NullRiskListException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new NullRiskListException();
            var messageResult = new NullRiskListException(message);
            var messageWithInnerExceptionResult = new NullRiskListException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void StartPolicyDateInPastException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new StartPolicyDateInPastException();
            var messageResult = new StartPolicyDateInPastException(message);
            var messageWithInnerExceptionResult = new StartPolicyDateInPastException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void StartRiskDateInPastException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new StartRiskDateInPastException();
            var messageResult = new StartRiskDateInPastException(message);
            var messageWithInnerExceptionResult = new StartRiskDateInPastException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void PolicyNotFoundException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new PolicyNotFoundException();
            var messageResult = new PolicyNotFoundException(message);
            var messageWithInnerExceptionResult = new PolicyNotFoundException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void NegativeYearlyPriceException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new NegativeYearlyPriceException();
            var messageResult = new NegativeYearlyPriceException(message);
            var messageWithInnerExceptionResult = new NegativeYearlyPriceException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void NonUniqueRiskInCollectionException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new NonUniqueRiskInCollectionException();
            var messageResult = new NonUniqueRiskInCollectionException(message);
            var messageWithInnerExceptionResult = new NonUniqueRiskInCollectionException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void RiskNameIsNullOrEmptyException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new RiskNameIsNullOrEmptyException();
            var messageResult = new RiskNameIsNullOrEmptyException(message);
            var messageWithInnerExceptionResult = new RiskNameIsNullOrEmptyException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void RiskNotFoundException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new RiskNotFoundException();
            var messageResult = new RiskNotFoundException(message);
            var messageWithInnerExceptionResult = new RiskNotFoundException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }

        [Fact]
        public void RiskWithValidEffectivePeriodNotFoundException_Constructors()
        {
            // arrange
            var message = _fixture.Create<string>();
            var innerException = _fixture.Create<Exception>();

            // act
            var parameterLessResult = new RiskWithValidEffectivePeriodNotFoundException();
            var messageResult = new RiskWithValidEffectivePeriodNotFoundException(message);
            var messageWithInnerExceptionResult = new RiskWithValidEffectivePeriodNotFoundException(message, innerException);

            // assert
            Assert.NotEqual(message, parameterLessResult.Message);
            Assert.Equal(message, messageResult.Message);
            Assert.Equal(message, messageWithInnerExceptionResult.Message);
            Assert.Equal(innerException, messageWithInnerExceptionResult.InnerException);
        }
    }
}
