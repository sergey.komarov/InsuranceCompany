﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class RiskWithValidEffectivePeriodNotFoundException : Exception
    {
        public RiskWithValidEffectivePeriodNotFoundException()
        {
        }

        public RiskWithValidEffectivePeriodNotFoundException(string message)
            : base(message)
        {
        }

        public RiskWithValidEffectivePeriodNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
