﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class EndRiskDateOutOfEffectivePeriodException : Exception
    {
        public EndRiskDateOutOfEffectivePeriodException()
        {
        }

        public EndRiskDateOutOfEffectivePeriodException(string message)
            : base(message)
        {
        }

        public EndRiskDateOutOfEffectivePeriodException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
