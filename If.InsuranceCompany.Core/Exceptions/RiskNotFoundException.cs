﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class RiskNotFoundException : Exception
    {
        public RiskNotFoundException()
        {
        }

        public RiskNotFoundException(string message)
            : base(message)
        {
        }

        public RiskNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
