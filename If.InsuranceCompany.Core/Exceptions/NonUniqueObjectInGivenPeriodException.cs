﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class NonUniqueObjectInGivenPeriodException : Exception
    {
        public NonUniqueObjectInGivenPeriodException()
        {
        }

        public NonUniqueObjectInGivenPeriodException(string message)
            : base(message)
        {
        }

        public NonUniqueObjectInGivenPeriodException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
