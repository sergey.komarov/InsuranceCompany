﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class NullRiskListException : Exception
    {
        public NullRiskListException()
        {
        }

        public NullRiskListException(string message)
            : base(message)
        {
        }

        public NullRiskListException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
