﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class StartPolicyDateInPastException : Exception
    {
        public StartPolicyDateInPastException()
        {
        }

        public StartPolicyDateInPastException(string message)
            : base(message)
        {
        }

        public StartPolicyDateInPastException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
