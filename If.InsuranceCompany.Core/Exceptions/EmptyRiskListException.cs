﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class EmptyRiskListException : Exception
    {
        public EmptyRiskListException()
        {
        }

        public EmptyRiskListException(string message)
            : base(message)
        {
        }

        public EmptyRiskListException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
