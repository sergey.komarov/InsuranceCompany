﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class NonUniqueRiskInCollectionException : Exception
    {
        public NonUniqueRiskInCollectionException()
        {
        }

        public NonUniqueRiskInCollectionException(string message)
            : base(message)
        {
        }

        public NonUniqueRiskInCollectionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
