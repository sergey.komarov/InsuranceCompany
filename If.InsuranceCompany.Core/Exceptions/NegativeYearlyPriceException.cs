﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class NegativeYearlyPriceException : Exception
    {
        public NegativeYearlyPriceException()
        {
        }

        public NegativeYearlyPriceException(string message)
            : base(message)
        {
        }

        public NegativeYearlyPriceException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
