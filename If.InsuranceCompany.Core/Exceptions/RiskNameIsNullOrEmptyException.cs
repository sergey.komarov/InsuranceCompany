﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class RiskNameIsNullOrEmptyException : Exception
    {
        public RiskNameIsNullOrEmptyException()
        {
        }

        public RiskNameIsNullOrEmptyException(string message)
            : base(message)
        {
        }

        public RiskNameIsNullOrEmptyException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
