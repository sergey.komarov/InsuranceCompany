﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class StartRiskDateInPastException : Exception
    {
        public StartRiskDateInPastException()
        {
        }

        public StartRiskDateInPastException(string message)
            : base(message)
        {
        }

        public StartRiskDateInPastException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
