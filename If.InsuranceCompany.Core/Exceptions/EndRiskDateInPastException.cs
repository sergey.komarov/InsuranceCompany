﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class EndRiskDateInPastException : Exception
    {
        public EndRiskDateInPastException()
        {
        }

        public EndRiskDateInPastException(string message)
            : base(message)
        {
        }

        public EndRiskDateInPastException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
