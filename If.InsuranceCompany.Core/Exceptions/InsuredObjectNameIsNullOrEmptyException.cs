﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class InsuredObjectNameIsNullOrEmptyException : Exception
    {
        public InsuredObjectNameIsNullOrEmptyException()
        {
        }

        public InsuredObjectNameIsNullOrEmptyException(string message)
            : base(message)
        {
        }

        public InsuredObjectNameIsNullOrEmptyException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
