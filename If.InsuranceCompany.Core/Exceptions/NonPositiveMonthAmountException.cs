﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class NonPositiveMonthAmountException : Exception
    {
        public NonPositiveMonthAmountException()
        {
        }

        public NonPositiveMonthAmountException(string message)
            : base(message)
        {
        }

        public NonPositiveMonthAmountException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
