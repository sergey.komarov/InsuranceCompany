﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class PolicyNotFoundException : Exception
    {
        public PolicyNotFoundException()
        {
        }

        public PolicyNotFoundException(string message)
            : base(message)
        {
        }

        public PolicyNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
