﻿using System;

namespace If.InsuranceCompany.Core.Exceptions
{
    public class NotAvailableRiskException : Exception
    {
        public NotAvailableRiskException()
        {
        }

        public NotAvailableRiskException(string message)
            : base(message)
        {
        }

        public NotAvailableRiskException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
