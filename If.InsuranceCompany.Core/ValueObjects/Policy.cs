﻿using System;
using System.Collections.Generic;
using System.Linq;
using If.InsuranceCompany.Core.ValueTypes;

namespace If.InsuranceCompany.Core.ValueObjects
{
    public class Policy : IPolicy
    {
        public Policy()
        {
            
        }

        public Policy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks)
        {
            NameOfInsuredObject = nameOfInsuredObject;
            ValidFrom = validFrom;
            ValidTill = ValidFrom.AddMonths(validMonths);
            InsuredRisks = selectedRisks;

            ActualInsuredRisks =
                selectedRisks.Select(x => new InsuredRisk(x, validFrom, validFrom.AddMonths(validMonths))).ToList();
        }

        public string NameOfInsuredObject { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTill { get; set; }
        public decimal Premium { get; set; }
        public IList<Risk> InsuredRisks { get; set; }

        /// <summary>
        /// list of risk that actual for this police
        /// TODO: to observable collection and recalculate premium on changes
        /// </summary>
        public IList<InsuredRisk> ActualInsuredRisks { get; set; }

        /// <summary>
        /// the period when policy is active
        /// </summary>
        public DateRange EffectivePeriod => new DateRange(ValidFrom, ValidTill);
    }

    public class InsuredRisk
    {
        public InsuredRisk(Risk risk, DateTime validFrom, DateTime validTill)
        {
            Risk = risk;
            ValidFrom = validFrom;
            ValidTill = validTill;
        }

        public Risk Risk { get; set; }

        /// <summary>
        /// date when risk insurance started
        /// </summary>
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// date when risk insurance ended
        /// </summary>
        public DateTime ValidTill { get; set; }

        /// <summary>
        /// the period when risk is active
        /// </summary>
        public DateRange EffectivePeriod => new DateRange(ValidFrom, ValidTill);
    }
}

