﻿using System;

namespace If.InsuranceCompany.Core.ValueObjects
{
    public class DateRange : IRange<DateTime>
    {
        public DateRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public DateRange(DateTime start, short months)
        {
            Start = start;
            End = start.AddMonths(months);
        }

        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public bool Includes(DateTime value)
        {
            return (Start <= value) && (value <= End);
        }

        public bool Includes(IRange<DateTime> range)
        {
            return (Start <= range.Start) && (range.End <= End);
        }

        public bool Overlaps(IRange<DateTime> range)
        {
            return ((Start < range.Start) && (range.End < End))
                   || ((Start < range.Start) && (End > range.Start))
                   || ((Start < range.End) && (End > range.End))
                   || ((Start > range.Start) && (range.End > End));
        }
    }
}
