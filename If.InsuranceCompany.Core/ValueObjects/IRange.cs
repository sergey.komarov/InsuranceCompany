﻿namespace If.InsuranceCompany.Core.ValueObjects
{
    public interface IRange<T>
    {
        T Start { get; }
        T End { get; }
        bool Includes(T value);
        bool Includes(IRange<T> range);
        bool Overlaps(IRange<T> range);
    }
}
