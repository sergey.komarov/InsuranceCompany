﻿using System;
using If.InsuranceCompany.Core.Exceptions;

namespace If.InsuranceCompany.Core.ValueTypes
{
    /// <summary>
    /// Notes: value type should be immutable but we have such struct in task
    /// </summary>
    public struct Risk : IEquatable<Risk>
    {
        private string _name;
        private decimal _yearlyPrice;

        public Risk(string name, decimal yearlyPrice) : this()
        {
            Name = name;
            YearlyPrice = yearlyPrice;
        }

        /// <summary>
        /// Unique name of the risk
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new RiskNameIsNullOrEmptyException();
                }

                _name = value;
            }
        }

        /// <summary>
        /// Risk yearly price
        /// </summary>
        public decimal YearlyPrice
        {
            get => _yearlyPrice;
            set
            {
                if (value < 0)
                {
                    throw new NegativeYearlyPriceException();
                }

                _yearlyPrice = value;
            }
        }

        public bool Equals(Risk other)
        {
            return string.Equals(_name, other._name) && _yearlyPrice == other._yearlyPrice;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Risk other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_name.GetHashCode() * 397) ^ _yearlyPrice.GetHashCode();
            }
        }
    }
}
