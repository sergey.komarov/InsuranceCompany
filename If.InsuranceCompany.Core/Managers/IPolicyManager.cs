﻿using System;
using System.Collections.Generic;
using If.InsuranceCompany.Core.ValueObjects;
using If.InsuranceCompany.Core.ValueTypes;

namespace If.InsuranceCompany.Core.Managers
{
    public interface IPolicyManager
    {
        bool PolicyExistsInGivenPeriod(string nameOfInsuredObject, DateTime validFrom, short validMonths);
        bool PolicyExistsOnEffectiveDate(string nameOfInsuredObject, DateTime effectiveDate);
        IPolicy CreatePolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks);
        Policy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate);
        void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom, DateTime effectiveDate);
        void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill, DateTime effectiveDate);
    }
}
