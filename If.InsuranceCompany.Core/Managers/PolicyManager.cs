﻿using System;
using System.Collections.Generic;
using System.Linq;
using If.InsuranceCompany.Core.Exceptions;
using If.InsuranceCompany.Core.ValueObjects;
using If.InsuranceCompany.Core.ValueTypes;

namespace If.InsuranceCompany.Core.Managers
{
    public class PolicyManager : IPolicyManager
    {
        private const int YearDaysMethod = 360;

        private readonly IList<Policy> _policies;

        public PolicyManager()
        {
            _policies = new List<Policy>();
        }

        public PolicyManager(IList<Policy> policies)
        {
            _policies = policies;
        }

        public IList<Policy> Policies => _policies;

        public bool PolicyExistsInGivenPeriod(string nameOfInsuredObject, DateTime validFrom, short validMonths)
        {
            return _policies.Any(x =>
                x.NameOfInsuredObject.Equals(nameOfInsuredObject)
                && x.EffectivePeriod.Overlaps(new DateRange(validFrom, validMonths)));
        }

        public bool PolicyExistsOnEffectiveDate(string nameOfInsuredObject, DateTime effectiveDate)
        {
            return _policies.Any(x =>
                x.NameOfInsuredObject.Equals(nameOfInsuredObject)
                && x.EffectivePeriod.Includes(effectiveDate));
        }

        public IPolicy CreatePolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks)
        {
            if (string.IsNullOrEmpty(nameOfInsuredObject))
                throw new InsuredObjectNameIsNullOrEmptyException();

            if (validMonths <= 0)
                throw new NonPositiveMonthAmountException();

            if (selectedRisks is null)
                throw new NullRiskListException();

            if (!selectedRisks.Any())
                throw new EmptyRiskListException();

            if (validFrom < DateTime.Now)
                throw new StartPolicyDateInPastException();

            if (_policies.Any(x =>
                x.NameOfInsuredObject.Equals(nameOfInsuredObject)
                && x.EffectivePeriod.Overlaps(new DateRange(validFrom, validMonths))))
                throw new NonUniqueObjectInGivenPeriodException();

            var policy = new Policy(nameOfInsuredObject, validFrom, validMonths, selectedRisks);
            policy.Premium = CalculatePremium(policy);
            _policies.Add(policy);
            return policy;
        }

        public Policy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate)
        {
            var policy = _policies.Where(x =>
                x.NameOfInsuredObject.Equals(nameOfInsuredObject)
                && x.EffectivePeriod.Includes(effectiveDate))
                .ToList();

            if (!policy.Any())
                throw new PolicyNotFoundException();

            return policy.SingleOrDefault();
        }

        public void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom, DateTime effectiveDate)
        {
            if (validFrom < DateTime.Now)
                throw new StartRiskDateInPastException();

            var policy = GetPolicy(nameOfInsuredObject, effectiveDate);
            policy.ActualInsuredRisks.Add(new InsuredRisk(risk, validFrom, policy.ValidTill));
            policy.Premium = CalculatePremium(policy);
        }

        public void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill, DateTime effectiveDate)
        {
            if (validTill < DateTime.Now)
                throw new EndRiskDateInPastException();

            var policy = GetPolicy(nameOfInsuredObject, effectiveDate);

            if (!policy.ActualInsuredRisks.Any(x => x.Risk.Equals(risk)))
                throw new RiskNotFoundException();

            if (!policy.ActualInsuredRisks.Any(x => x.EffectivePeriod.Includes(validTill)))
                throw new EndRiskDateOutOfEffectivePeriodException();

            var insuredRisk = policy.ActualInsuredRisks.FirstOrDefault(x => x.Risk.Equals(risk) && x.EffectivePeriod.Includes(validTill));
            if (insuredRisk is null)
                throw new RiskWithValidEffectivePeriodNotFoundException();

            insuredRisk.ValidTill = validTill;
            policy.Premium = CalculatePremium(policy);
        }

        public decimal CalculatePremium(Policy policy)
        {
            return policy
                .ActualInsuredRisks
                .Select(x => (x.ValidTill - x.ValidFrom).Days * x.Risk.YearlyPrice / YearDaysMethod)
                .Sum();
        }
    }
}
