﻿using System;
using System.Collections.Generic;
using System.Linq;
using If.InsuranceCompany.Core.Exceptions;
using If.InsuranceCompany.Core.Managers;
using If.InsuranceCompany.Core.ValueObjects;
using If.InsuranceCompany.Core.ValueTypes;

namespace If.InsuranceCompany.Core
{
    public class InsuranceCompany : IInsuranceCompany
    {
        private readonly IPolicyManager _policyManager;

        private IList<Risk> _availableRisks;

        public InsuranceCompany(IPolicyManager policyManager)
        {
            AvailableRisks = new List<Risk>();
            _policyManager = policyManager;
        }

        public string Name => "Acme Insurance Corporation";

        public IList<Risk> AvailableRisks
        {
            get
            {
                if (_availableRisks.GroupBy(x => x.Name).Any(x => x.Count() > 1))
                    throw new NonUniqueRiskInCollectionException();

                return _availableRisks;
            }
            set => _availableRisks = value;
        }

        public IPolicy SellPolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths, IList<Risk> selectedRisks)
        {
            if (selectedRisks.Any(x => !AvailableRisks.Contains(x)))
                throw new NotAvailableRiskException();

            return _policyManager.CreatePolicy(nameOfInsuredObject, validFrom, validMonths, selectedRisks);
        }

        public void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom, DateTime effectiveDate)
        {
            if (!AvailableRisks.Contains(risk))
                throw new NotAvailableRiskException();

            _policyManager.AddRisk(nameOfInsuredObject, risk, validFrom, effectiveDate);
        }

        public void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill, DateTime effectiveDate)
        {
            _policyManager.RemoveRisk(nameOfInsuredObject, risk, validTill, effectiveDate);
        }

        public IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate)
        {
            return _policyManager.GetPolicy(nameOfInsuredObject, effectiveDate);
        }
    }
}
